# Janitor Run

## Objective
Create simple 3D multiplayer game for the ~1 km long central hallway of Virtual Campus. Players control an avatar of a janitor on a kickboard. Weather and university traffic/crowd will be reflected in near real time in the game scene and utilized in the game design.

## Motivation
Finding new practical ways to use real world elements and events in game concept and binding them to virtual world